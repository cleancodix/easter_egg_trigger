## An Easter Egg trigger widget

This ***Easter egg*** *trigger* widget allow you to easily call a function
when a combination of gestures have been detected on a given widget

- Please follow this link to find a working and concrete example :
<a href="https://gitlab.com/mrdiez/flutter_scaffold" target="_blank">The Flutter Scaffold by Mr#</a>
And look at the **./lib/components/images/main.logo.dart** file

- Customizable ! Just provide a ***List<EasterEggTriggers>*** to add your own *gestures*

- It can't be easier to use :
```
    EasterEggTrigger(
      action: () => print("Easter Egg !!!"),
      child: Image.network('https://picsum.photos/250?image=9')
    );
```

This example will print **Easter Egg !!!** in your console
when you do these *gestures* on the image:

  - >Swipe Up, Swipe Up, Swipe Down, Swipe Down,
  - >Swipe Left, Swipe Right, Swipe Left, Swipe Right,
  - >Tap, Long Press

Yes by default this is the famous ***Konami*** *code*: **↑ ↑ ↓ ↓ ← → ← → B A**
note that I replaced B button by Tap and A button by Long Press

